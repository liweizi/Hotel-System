<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;

class System extends Base
{
    /**
     * 首页
     *
     * @return \think\Response
     */
    public function index()
    {
       echo '2222222';
    }

    /**
     * 语音设置、账号设置
     * @return \think\Response
     */
    public function voice()
    {
        if(request()->isPost()){
            $this->speech(input('apiKey'),input('secretKey'),input('people'),input('speed'),input('tone'),input('volume'),'欢迎使用布尔酒店管理系统','bool');
            $this->redirect('/api/tts.php');
        }
        $res =$this->reads('voice_config');
        $list = Db::table('hotel_voice')->select();
        $this->assign([
            'res' => $res,
            'list' => $list
        ]);
        return view();
    }

    /**
     * 修改播报信息和文件名
     *
     * @return \think\Response
     */
    public function revise()
    {
        if(request()->isPost()){
            Db::table('hotel_voice')->update(input('param.'));
            $this->redirect('/admin/system/voice');
        }
        $list = Db::table('hotel_voice')->where('id',input('id'))->find();
        $this->assign('list',$list);
        return view();
    }
    /**
     * 生成新的语音
     *
     * @return \think\Response
     */
    public function renew()
    {
        $list = Db::table('hotel_voice')->where('id',input('id'))->find();
        $res =$this->reads('voice_config');
        $this->speech($res[0],$res[1],$res[2],$res[3],$res[4],$res[5],$list['text'],$list['card']);
        $this->redirect('/api/tts.php');
    }
    /**
     * 语音合成
     * $per 发音人选择, 0为普通女声，1为普通男生，3为情感合成-度逍遥，4为情感合成-度丫丫，默认为普通女声
     * $spd 语速，取值0-15，默认为5中语速
     * $pit 音调，取值0-15，默认为5中语调
     * $vol 音量，取值0-9，默认为5中音量
     * $text 语音默认的内容为"欢迎使用百度语音合成"的urlencode,utf-8 编码
     * $voice 保存语音文件的名称
     */
    public function speech($apiKey,$secretKey,$per,$spd,$pit,$vol,$text,$voice)
    {
        $data=[
            'apiKey'=>$apiKey.',',
            'secretKey'=>$secretKey.',',
            'per'=>$per.',',
            'spd'=>$spd.',',
            'pit'=>$pit.',',
            'vol'=>$vol.',',
            'text'=>$text.',',
            '$voice'=>$voice
        ];
        $this->writes('voice_config',$data);
    }
    /**
     * 房间管理
     *
     * @return \think\Response
     */
    public function rooms()
    {
        return view();
    }

    /**
     * 添加房间
     *
     * @return \think\Response
     */
    public function add_house()
    {
        if(request()->isPost()){
            return $this->inserts('house',input('param.'));
        }
        return view();
    }

    /**
     * 渠道设置
     *
     * @return \think\Response
     */
    public function channel()
    {
        if(request()->isPost()){
            return $this->inserts('channel',input('param.'));
        }
        $this->query('hotel_channel','list');
        return view();
    }

    /**
     * 删除渠道
     *
     * @return \think\Response
     */
    public function remove()
    {
        return $this->deletes('hotel_channel',input('id'));
    }

    /**
     * 价格方案
     *
     * @return \think\Response
     */
    public function scheme()
    {
        return view();
    }
    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
