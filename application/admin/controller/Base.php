<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use think\Request;

class Base extends Controller
{


    /**
     * 写入日志文件
     * $name 写入的文件名
     * $data 需要写入的数据
     */
    public function writes($name,$data)
    {
        file_put_contents('./config/'.$name.'.txt',$data);
    }

    /**
     * 读取日志文件
     * $name 读取的文件名
     */
    public function reads($name)
    {
        $content = file_get_contents('./config/'.$name.".txt");
        $res = explode(",", $content);
        return $res;
    }
    /**
     * 查询多条数据
     *
     * @return \think\Response
     */
    public function query($tables,$data)
    {
        $list = Db::table($tables)->select();
        $this->assign($data,$list);
    }

    /**
     * 按条件查询1条数据
     *
     * @return \think\Response
     */
    public function query_find($tables,$map,$data)
    {
        $list = Db::table($tables)->where($map)->find();
        $this->assign($data,$list);
    }

    /**
     * 添加数据
     *
     * @return \think\Response
     */
    public function inserts($tables,$data)
    {
        return $this->return_json( Db::name($tables)->insert($data));
    }

    /**
     * 返回json数据
     *
     * @return \think\Response
     */
    public function return_json($res)
    {
        if($res){
            return json([
                'msg' => '操作成功',
                'code' => 100
            ]);
        }else{
            return json([
                'msg' => '操作失败',
                'code' => 0
            ]);
        }
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function deletes($tables,$id)
    {
        return $this->return_json(Db::table($tables)->where('id',$id)->delete());
    }
}
